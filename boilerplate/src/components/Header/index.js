import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { increment } from 'store/actions';

import { header } from './header.scss';

const HeaderComponent = ({ title, counter, makeIncrement }) => (
  <div className={header}>
    <h1>
      {title}
    </h1>
    <div>{counter}</div>
    <button type="button" onClick={makeIncrement}>increment</button>
  </div>
);

HeaderComponent.propTypes = {
  title: PropTypes.string.isRequired,
  makeIncrement: PropTypes.func.isRequired,
  counter: PropTypes.number.isRequired,
};

const mapStateToProps = state => ({
  counter: state.counter,
});
const mapDispatchToProps = dispatch => ({
  makeIncrement: () => dispatch(increment),
});

export const Header = connect(mapStateToProps, mapDispatchToProps)(HeaderComponent);
